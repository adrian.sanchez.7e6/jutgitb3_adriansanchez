import org.sqlite.SQLiteException
import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.Statement



fun menuEstudiant() {
    println("MENÚ D'ESTUDIANT")
    println("1. Seguir amb l'itinerari d'aprenentatge")
    println("2. Llista de problemes i triar-ne un")
    println("3. Consultar històric de problemes resolts")
    println("4. Ajuda")
    println("0. Sortir")
}

fun menuProfessor() {
    println("MENÚ DEL PROFESSOR")
    println("1. Afegir nous problemes")
    println("2. Treure un report de la feina de l'alumne")
    println("0. Sortir")
}

fun submenuProfessorOpcio2() {
    println("SUBMENÚ OPCIÓ 2")
    println("1. Treure una puntuació en funció dels problemes resolts")
    println("2. Descomptar per intents")
    println("3. Mostrar-ho de manera més o menys gràfica")
    println("0. Sortir")
}


fun main(args: Array<String>) {
    val dataBaseFile = "./bd/db_adrian.db"

    var nomUsuari = ""
    var idUsuari = 0
    var rolUsuari = ""

    println("Introdueix la ID del teu usuari:")
    var idIntroduidaUsuari = readln().toInt()
    DriverManager.getConnection("jdbc:sqlite:$dataBaseFile").use { connection ->
        val listStatement: Statement = connection.createStatement()
        val query = "SELECT * FROM usuario;"
        val resultat: ResultSet = listStatement.executeQuery(query)
        while (resultat.next()) {
            val id = resultat.getInt("usuarioId")
            val nom = resultat.getString("nombre")
            val contrasenya = resultat.getString("usuarioContraseña")
            val rol = resultat.getString("rol")
            if (id == idIntroduidaUsuari) {
                rolUsuari = rol
                nomUsuari = nom
                idUsuari = id
                println("Benvingut $nom, introdueix la teva contrasenya:")
                var contrasenyaUsuari = readln()
                while (contrasenyaUsuari != contrasenya) {
                    println("La contrasenya no és correcta, torna a introduir-la")
                    contrasenyaUsuari = readln()
                }
            }
        }
    }

    if (rolUsuari == "alumno") {
        do {
            menuEstudiant()
            println("Tria una opció:")
            var opcioMenuEstudiant = readln().toInt()
            while (opcioMenuEstudiant < 0 || opcioMenuEstudiant > 4) {
                println("La opció seleccionada ha d'estar entre 0 i 4")
                opcioMenuEstudiant = readln().toInt()
            }
            when (opcioMenuEstudiant) {
                1 -> {
                    println("Has triat seguir amb l'itinerari d'aprenentatge")
                    DriverManager.getConnection("jdbc:sqlite:$dataBaseFile").use { connection ->
                        val listStatement: Statement = connection.createStatement()
                        val query = "SELECT * FROM PROBLEMA;"
                        val resultat: ResultSet = listStatement.executeQuery(query)
                        while (resultat.next()) {
                            val enunciat = resultat.getString("enunciado")
                            val idProblema = resultat.getInt("problemaId")
                            val resolt = resultat.getString("solucion")
                            if (resolt == "false") {
                                val listStatement: Statement = connection.createStatement()
                                val query = "SELECT * FROM JUEGO_DE_PRUEBAS WHERE problemaId=$idProblema;"
                                val resultat: ResultSet = listStatement.executeQuery(query)
                                while (resultat.next()) {
                                    val inputPublic = resultat.getString("inputPublico")
                                    val outputPublic = resultat.getInt("ouputPublico")
                                    val inputPrivat = resultat.getString("inputPrivado")
                                    val outputPrivat = resultat.getInt("outputPrivado")

                                    println(enunciat)
                                    println("Input del joc de proves públic:")
                                    println(inputPublic)
                                    println("Output del joc de proves públic:")
                                    println(outputPublic)
                                    println("Input del joc de proves privat:")
                                    println(inputPrivat)
                                    println("Introdueix el resultat:")
                                    do {
                                        var resultatUsuari = readln().toInt()
                                        val listStatement: Statement = connection.createStatement()
                                        val query =
                                            "INSERT INTO INTENTOS (problemaId, usuarioId, inputUsuario) VALUES ('$idProblema', '$idUsuari', '$resultatUsuari');"
                                        var statement = connection.prepareStatement(query)
                                        statement.execute()
                                        if (resultatUsuari == outputPrivat) {
                                            val listStatement: Statement = connection.createStatement()
                                            val query =
                                                "UPDATE PROBLEMA SET solucion='true' WHERE problemaId=$idProblema;"
                                            var statement = connection.prepareStatement(query)
                                            statement.execute()
                                        }
                                    } while (resultatUsuari != outputPrivat)


                                }

                            } else {
                                println("El problema $idProblema està resolt")
                            }
                        }

                    }
                }

                2 -> {
                    DriverManager.getConnection("jdbc:sqlite:$dataBaseFile").use { connection ->
                        val listStatement: Statement = connection.createStatement()
                        var consultaNumeroProblemes = "SELECT * FROM PROBLEMA ORDER BY problemaId DESC LIMIT 1;"
                        val numProblemes: ResultSet = listStatement.executeQuery(consultaNumeroProblemes)

                        var numeroTotalProblemes = numProblemes.getInt("problemaId")

                        println("Tria un problema per a intentar resoldre'l, el número ha d'estar entre 1 i $numeroTotalProblemes")
                        var triarProblema = readln().toInt()
                        while (triarProblema < 1 || triarProblema > numeroTotalProblemes) {
                            println("El número ha d'estar entre 1 i $numeroTotalProblemes")
                            triarProblema = readln().toInt()
                        }


                        val query = "SELECT * FROM PROBLEMA WHERE problemaId=$triarProblema;"
                        val resultat: ResultSet = listStatement.executeQuery(query)
                        val enunciat = resultat.getString("enunciado")
                        val idProblema = resultat.getInt("problemaId")
                        val resolt = resultat.getString("solucion")
                        if (resolt == "false") {
                            val listStatement: Statement = connection.createStatement()
                            val query = "SELECT * FROM JUEGO_DE_PRUEBAS WHERE problemaId=$idProblema;"
                            val resultat: ResultSet = listStatement.executeQuery(query)
                            val inputPublic = resultat.getString("inputPublico")
                            val outputPublic = resultat.getInt("ouputPublico")
                            val inputPrivat = resultat.getString("inputPrivado")
                            val outputPrivat = resultat.getInt("outputPrivado")

                            println(enunciat)
                            println("Input del joc de proves públic:")
                            println(inputPublic)
                            println("Output del joc de proves públic:")
                            println(outputPublic)
                            println("Input del joc de proves privat:")
                            println(inputPrivat)
                            println("Introdueix el resultat:")
                            do {
                                var resultatUsuari = readln().toInt()
                                val listStatement: Statement = connection.createStatement()
                                val query =
                                    "INSERT INTO INTENTOS (problemaId, usuarioId, inputUsuario) VALUES ('$idProblema', '$idUsuari', '$resultatUsuari');"
                                var statement = connection.prepareStatement(query)
                                statement.execute()
                                if (resultatUsuari == outputPrivat) {
                                    val listStatement: Statement = connection.createStatement()
                                    val query = "UPDATE PROBLEMA SET solucion='true' WHERE problemaId=$idProblema;"
                                    var statement = connection.prepareStatement(query)
                                    statement.execute()
                                }
                            } while (resultatUsuari != outputPrivat)
                        } else println("El problema $triarProblema ja està resolt")
                    }
                }

                3 -> {
                    println("Has triat consultar històric de problemes")
                    DriverManager.getConnection("jdbc:sqlite:$dataBaseFile").use { connection ->
                        val listStatement: Statement = connection.createStatement()
                        val query = "SELECT * FROM PROBLEMA;"
                        val resultat: ResultSet = listStatement.executeQuery(query)
                        while (resultat.next()) {
                            var numeroProblema = resultat.getInt("problemaId")
                            var resolt = resultat.getString("solucion")
                            if (resolt != "false") {
                                println("\u001b[42mPROBLEMA $numeroProblema\u001b[0m")
                                println("El problema $numeroProblema ha estat resolt")
                            } else {
                                println("\u001b[43mPROBLEMA $numeroProblema\u001b[0m")
                                println("El problema $numeroProblema no ha estat resolt")

                            }
                        }
                    }
                }

                4 -> {
                    println("Ajuda")
                    println("En aquest apartat veuràs quina funció té cada opció de menú")
                    println("Opció 1. Seguiràs amb l'itinerari d'aprenentatge, és a dir, et mostrarà per ordre els problemes que et queden per resoldre")
                    println("Opció 2. Et mostrarà un llistat amb els enunciats dels problemes disponibles i podràs triar quin vols resoldre")
                    println("Opció 3. Et mostra quins problemes han estat resolts i quins no")
                    println("Opció 4. Mostra l'ajuda del programa")
                    println("Opció 0. Sortir")

                }

                0 -> println("Fins aviat!")
            }
        } while (opcioMenuEstudiant != 0)

    } else if (rolUsuari == "profesor") {
        do {
            menuProfessor()
            println("Tria una opció:")
            var opcioMenuProfessor = readln().toInt()
            while (opcioMenuProfessor < 0 || opcioMenuProfessor > 2) {
                println("La opció seleccionada ha d'estar entre 0 i 2")
                opcioMenuProfessor = readln().toInt()
            }
            when (opcioMenuProfessor) {
                1 -> {
                    println("Has triat afegir un nou problema, introdueix els següents atributs:")
                    DriverManager.getConnection("jdbc:sqlite:$dataBaseFile").use { connection ->
                        val listStatement: Statement = connection.createStatement()
                        print("Enunciat: ")
                        var enunciat = readln().toString()
                        val query = "INSERT INTO PROBLEMA (enunciado) VALUES ('$enunciat');"
                        var statement = connection.prepareStatement(query)
                        statement.execute()
                    }

                    DriverManager.getConnection("jdbc:sqlite:$dataBaseFile").use { connection ->
                        val listStatement: Statement = connection.createStatement()
                        var consultaNumeroProblemes = "SELECT * FROM PROBLEMA ORDER BY problemaId DESC LIMIT 1;"
                        val numProblemes: ResultSet = listStatement.executeQuery(consultaNumeroProblemes)
                        var numeroTotalProblemes = numProblemes.getInt("problemaId")

                        print("Input públic: ")
                        var inputPublic = readln().toString()
                        print("Output públic: ")
                        var outputPublic = readln().toInt()
                        print("Input privat: ")
                        var inputPrivat = readln().toString()
                        print("Output privat: ")
                        var outputPrivat = readln().toInt()
                        val query =
                            "INSERT INTO JUEGO_DE_PRUEBAS (juegoId, inputPublico, ouputPublico, inputPrivado, outputPrivado, problemaId) VALUES ('$numeroTotalProblemes', '$inputPublic', '$outputPublic', '$inputPrivat', '$outputPrivat', '$numeroTotalProblemes');"
                        var statement = connection.prepareStatement(query)
                        statement.execute()
                    }
                }

                2 -> {
                    do {
                        submenuProfessorOpcio2()
                        println("Tria una opció:")
                        var opcioSubmenu = readln().toInt()
                        while (opcioSubmenu < 0 || opcioSubmenu > 3) {
                            println("La opció seleccionada ha d'estar entre 0 i 3")
                            opcioSubmenu = readln().toInt()
                        }
                        when (opcioSubmenu) {
                            1 -> {
                                var problemesResolts = 0
                                println("Has triat treure una puntuació en funció dels problemes resolts")
                                DriverManager.getConnection("jdbc:sqlite:$dataBaseFile").use { connection ->
                                    val listStatement: Statement = connection.createStatement()
                                    var consultaNumeroProblemes =
                                        "SELECT * FROM PROBLEMA ORDER BY problemaId DESC LIMIT 1;"
                                    val numProblemes: ResultSet = listStatement.executeQuery(consultaNumeroProblemes)
                                    var numeroTotalProblemes = numProblemes.getInt("problemaId")

                                    val query = "SELECT * FROM PROBLEMA;"
                                    val resultat: ResultSet = listStatement.executeQuery(query)
                                    while (resultat.next()) {
                                        var resolt = resultat.getString("solucion")
                                        if (resolt != "false") {
                                            problemesResolts++
                                        }
                                    }
                                    var percentatgeResolts =
                                        (problemesResolts.toDouble() / numeroTotalProblemes.toDouble()) * 100
                                    println("S'han resolt $problemesResolts de $numeroTotalProblemes problemes")
                                    println(
                                        "El percentatge de problemes resolts és del ${
                                            String.format(
                                                "%.2f",
                                                percentatgeResolts
                                            )
                                        }% del total"
                                    )
                                }
                            }

                            2 -> {
                                // AQUESTA PART NO LA HE SAPIGUT FER
                            }

                            3 -> {
                                DriverManager.getConnection("jdbc:sqlite:$dataBaseFile").use { connection ->
                                    val listStatement: Statement = connection.createStatement()
                                    val query = "SELECT * FROM PROBLEMA;"
                                    val resultat: ResultSet = listStatement.executeQuery(query)
                                    while (resultat.next()) {
                                        var numeroProblema = resultat.getInt("problemaId")
                                        var resolt = resultat.getString("solucion")
                                        if (resolt != "false") {
                                            println("\u001b[42mPROBLEMA $numeroProblema\u001b[0m")
                                            println("El problema $numeroProblema ha estat resolt")
                                        } else {
                                            println("\u001b[43mPROBLEMA $numeroProblema\u001b[0m")
                                            println("El problema $numeroProblema no ha estat resolt")

                                        }
                                    }
                                }
                            }
                        }
                    } while (opcioSubmenu != 0)
                }
                0 -> println("Fins aviat!")
            }
        } while (opcioMenuProfessor != 0)
    }
}